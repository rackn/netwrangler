module gitlab.com/rackn/netwrangler

go 1.16

require (
	github.com/ghodss/yaml v1.0.0
	gitlab.com/rackn/gohai v0.7.12
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
